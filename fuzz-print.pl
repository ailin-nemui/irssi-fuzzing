# perl script to fuzz irssi
# by Hanno Böck, license: CC0 / public domain
#
# Will read fuzzp.txt and print it.
# Use with afl by putting it into irssi's autorun directory (default: ~/.irssi/scripts/autorun)
# and run something like:
#   afl-fuzz -i in -o out -m none -t 5000 -f fuzzp.txt irssi
use Irssi;

open $file, "fuzzp.txt" or die "failed to open";
while ( my $line = <$file> ) {
    print $line;
}
close $file;
Irssi::command("QUIT");
