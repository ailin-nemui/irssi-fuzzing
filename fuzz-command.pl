# perl script to fuzz irssi
# by Hanno Böck, license: CC0 / public domain
#
# Will read fuzzc.txt and execute it as a command.
# Use with afl by putting it into irssi's autorun directory (default: ~/.irssi/scripts/autorun)
# and run something like:
#   afl-fuzz -i in -o out -m none -t 5000 -f fuzzc.txt irssi
use Irssi;

open $file, "fuzzc.txt" or die "failed to open";
while ( my $line = <$file> ) {
    Irssi::command($line);
}
close $file;
Irssi::command("QUIT");
